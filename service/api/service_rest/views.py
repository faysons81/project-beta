from datetime import date
from datetime import time
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO
from django.http import JsonResponse
import json


# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties =[
        'vin',
        'sold'
    ]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        'first_name',
        'last_name',
        'employee_id',
        "id"
    ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'date',
        'time',
        'reason',
        'status',
        'vin',
        'customer',
        'technician',
        "id",
        'vip'
    ]
    encoders = {
       
        'technician': TechnicianListEncoder()
    }
    def default(self, obj):
        if isinstance(obj, date):
            return obj.strftime('%Y-%m-%d')
        elif isinstance(obj, time):
            return obj.strftime('%H:%M:%S')
        return super().default(obj)


@require_http_methods(['GET', 'POST'])
def api_technician_list(request):
    if request.method == 'GET':
        technicians = Technician.objects.all()
        return JsonResponse(
            {'technicians': technicians},
            encoder=TechnicianListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False
        )

@require_http_methods(['GET', "POST",])
def api_appointment_list(request):
    if request.method == 'GET':
        appointments = Appointment.objects.all()
        return JsonResponse(
            {'appointments': appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician not found"},
                status=400,
            )
        

        if AutomobileVO.objects.filter(vin=content["vin"]).exists():
            content["vip"] = "Yes"
        content["status"] = "Created"
        appointment = Appointment.objects.create(**content)

        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentListEncoder,
            safe=False,
        )

@require_http_methods(["PUT"])
def api_appointment_finish(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = 'Finished'
        appointment.save()
        return JsonResponse({'message': 'Appointment marked as finished'})
    except Appointment.DoesNotExist:
        return JsonResponse({'error': 'Appointment not found'}, status=404)
    
@require_http_methods(["PUT"])
def api_appointment_cancel(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = 'Canceled'
        appointment.save()
        return JsonResponse({'message': 'Appointment marked as canceled'})
    except Appointment.DoesNotExist:
        return JsonResponse({'error': 'Appointment not found'}, status=404)

@require_http_methods(['GET'])
def api_show_history(request):
    if request.method == 'GET':
        appt_history = Appointment.objects.all()
        if appt_history == []:
            return JsonResponse(
                {'message': "invalid VIN"}
            )
        else:
            return JsonResponse(
                appt_history,
                AppointmentListEncoder,
                safe=False
            )