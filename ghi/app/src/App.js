import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import CreateSalesPerson from "./CreateSalesPerson";
import ListSalesPeople from "./ListSalesPeople";
import SaleForm from './NewSale';
import SalespersonHistory from './SalesPersonHistory';
import ListSales from './SalesList';
import AddCustomer from "./AddCustomer";
import ListCustomers from './ListCustomers';
import TechnicianList from "./Service/TechnicianList";
import TechnicianForm from "./Service/TechnicianForm";
import ManufacturerList from "./ManufacturerList";
import ManufacturerForm from "./ManufacturerForm";
import AutomobileList from "./AutomobileList";
import AutomobileForm from "./AutomobileForm";
import ModelForm from "./ModelForm";
import ModelList from "./ModelList";
import AppointmentForm from "./Service/AppointmentForm";
import AppointmentList from "./Service/AppointmentList";
import ServiceHistory from "./Service/ServiceHistory";



function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="addcustomer" element={<AddCustomer />} />
          <Route path="create" element={<CreateSalesPerson />} />
          <Route path="salespeople" element={<ListSalesPeople salespeople={props.salespeople} />}></Route>

          <Route
            path="/technicians"
            element={<TechnicianList />}
          />
          <Route
            path="/technicians/new"
            element={<TechnicianForm />}
          />
          <Route
          path="/appointments"
          element={<AppointmentList/>}
          />
          <Route
          path="/appointments/new"
          element={<AppointmentForm/>}
          />

          <Route
            path="/manufacturers"
            element={<ManufacturerList />}
          />
          <Route
            path="/manufacturers/create"
            element={<ManufacturerForm />}
          />
          <Route
          path="/automobiles"
          element={<AutomobileList/>}
          />
          <Route
          path="/automobiles/new"
          element={<AutomobileForm/>}
       />
       <Route
          path="/appointments/servicehistory"
          element={<ServiceHistory/>}
       />
       <Route
       path="/models"
       element={<ModelList/>}
       />
       <Route
          path="/models/new"
          element={<ModelForm/>}
          />

          <Route path ="/salesform" element={<SaleForm/>}/>
          <Route path="salespersonhistory" element={<SalespersonHistory />} />
          <Route path="customerlist" element={<ListCustomers customers={props.customers} />}></Route>
          <Route path="saleslist" element={<ListSales sales={props.sales} />}></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
