import React, { useState, useEffect } from 'react';


const AutomobileForm = () => {
    const [models, setModels] = useState([]);
    const [formData, setFormData] = useState({
        color: "",
        year: "",
        vin: "",
        model_id: 0
    })


    const getModels = async () => {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }

    const onHandleSubmit = async (e) => {
        e.preventDefault();
        const autoUrl = "http://localhost:8100/api/automobiles/";
        const data = JSON.stringify(formData);
        const fetchConfig = {
            method: "post",
            body: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                color: "",
                year: "",
                vin: "",
                model_id: 0
            })
        }console.log(formData)
    }


    const handleFormChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        setFormData({
            ...formData, [name]: value
        });
    }

    useEffect(() => {
        getModels();
    }, []);

    return (
        <div>
            <h1>Add an Automobile</h1>
            <form onSubmit={onHandleSubmit}>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{ width: 300 }}>
                    <input onChange={handleFormChange} required name="color" type="text" value={formData.color} placeholder="Color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{ width: 300 }}>
                    <input onChange={handleFormChange} name="year" type="text" value={formData.year} placeholder="Year" className="form-control" />
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{ width: 300 }}>
                    <input onChange={handleFormChange} name="vin" type="text" value={formData.vin} placeholder="VIN" className="form-control" />
                    <label htmlFor="vin">VIN</label>
                </div>
                <div className="form-group col-5 mx-auto mt-3" style={{ width: 300 }}>
                    <label className="" htmlFor="model">Model</label>
                    <select onChange={handleFormChange} required name="model_id" value={formData.model_id} className="form-control">
                        <option value="">Choose a model</option>
                        {models.map(model => {
                            return (
                                <option key={model.id} value={model.id}>{model.name}</option>
                            )
                        })}

                    </select>
                </div>
                <div className="d-grid form group col-5 mx-auto" style={{ width: 300 }}>
                    <button className="btn btn-primary btn-block mt-3">Add</button>
                </div>
            </form>
        </div>
    )
}
export default AutomobileForm
