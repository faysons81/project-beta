import { useState, useEffect } from "react";

export default function AddModel() {
    const [manufacturers, setManufacturers] = useState([]);
    const [formData, setFormData] = useState({
        name: "",
        picture_url: "",
        manufacturer_id: 0
    });

    const fetchManufacturers = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }

    const handleFormSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8100/api/models/";
        const data = JSON.stringify(formData);
        const fetchConfig = {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: "",
                picture_url: "",
                manufacturer_id: 0
            });
        }
    }

    const handleFormChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        setFormData({
            ...formData, [name]: value
        });
    }

    useEffect(() => {
        fetchManufacturers();
    }, []);

    return(
        <div className="backdrop-blur mt-5 mx-auto shadow pb-3 rounded-md" style={{width: 500}}>
            <div className="bg-gradient-to-l from-blue to-gray-dark rounded-t-md">
                <h1 className="text-white text-center text-2xl">Add a model</h1>
            </div>
            <form onSubmit={handleFormSubmit}>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{width: 300}}>
                    <input onChange={handleFormChange} required name="name" type="text" value={formData.name} placeholder="Name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{width: 300}}>
                    <input onChange={handleFormChange} name="picture_url" type="url" value={formData.picture_url} placeholder="Picture URL" className="form-control" />
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="form-group col-5 mx-auto mt-3" style={{width: 300}}>
                    <label className="" htmlFor="manufacturer_id">Manufacturer</label>
                    <select onChange={handleFormChange} required name="manufacturer_id" value={formData.manufacturer_id} className="form-control">
                        <option value="">Choose a manufacturer</option>
                        {manufacturers.map(manufacturer => {
                            return(
                                <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                            );
                        })}
                    </select>
                </div>
                <div className="d-grid form group col-5 mx-auto" style={{width: 300}}>
                    <button className="btn bg-blue hover:bg-gray btn-block mt-3">Add</button>
                </div>
            </form>
        </div>
    );
}
