import React, { useState, useEffect } from 'react';

function ModelList() {
    const [models, setModels] = useState([]);

    useEffect(() => {
        const fetchModels = async () => {
            try {
                const response = await fetch('http://localhost:8100/api/models/');
                if (response.ok) {
                    const data = await response.json();
                    setModels(data.models);
                } else {
                    throw new Error('Failed to fetch models');
                }
            } catch (error) {
                console.error('Error fetching models:', error);
            }
        };

        fetchModels();
    }, []);

    return (
        <div>
            <>
                <h1>Models</h1>
            </>
            <table className="table table-striped">
                <thead>
                    <tr className="table-success">
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id} value={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td><img src={model.picture_url} alt={model.name} width='100px'
                        height='100px'/></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>

    );
}

export default ModelList;
