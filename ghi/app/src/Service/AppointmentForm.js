import React, { useState, useEffect } from "react";

const AppointmentForm = () => {
  const [technicians, setTechnicians] = useState([]);
  const [formData, setFormData] = useState({
    vin: '',
    customer: '',
    date: '',
    time: '',
    technician: 0,
    reason: '',
  });

  const getTechs = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians)
    }
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(formData)
    try {
      const appointmentUrl = 'http://localhost:8080/api/appointments/';
      const data = JSON.stringify(formData);
      const fetchConfig = {
        method: 'post',
        body: data,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(appointmentUrl, fetchConfig);

      if (response.ok) {
        setFormData({
          vin: '',
          customer: '',
          date: '',
          time: '',
          technician: 0,
          reason: '',
        });
      }
      else {
        console.error('Failed to submit form:', response.statusText);
      }
    } catch (error) {
      console.error('Error submitting form:', error);
    }
  };

  const handleFormChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;

    setFormData({
      ...formData,
      [name]: value,
    });
  };

  useEffect(() => {
    getTechs();
  }, []);

  return (
    <div>
      <h1>Create a service appointment</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group form-floating col-5 mx-auto mt-3" style={{ width: 300 }}>
          <input onChange={handleFormChange} value={formData.vin} required name="vin" type="text" className="form-control" />
          <label htmlFor="vin">Automobile VIN</label>
        </div>
        <div className="form-group form-floating col-5 mx-auto mt-3" style={{ width: 300 }}>
          <input
            type="text"
            id="customer"
            name="customer"
            value={formData.customer}
            onChange={handleFormChange}
            required
            className="form-control"
          />
          <label htmlFor="customer">Customer</label>
        </div>
        <div className="form-group form-floating col-5 mx-auto mt-3" style={{ width: 300 }}>
          <input onChange={handleFormChange} value={formData.date} required name="date" type="date" className="form-control" />
          <label htmlFor="date">Date</label>
        </div>
        <div className="form-group form-floating col-5 mx-auto mt-3" style={{ width: 300 }}>
          <input onChange={handleFormChange} value={formData.time} required name="time" type="time" className="form-control" />
          <label htmlFor="time">Time</label>
        </div>
        <div className="form-group col-5 mx-auto mt-3" style={{ width: 300 }}>
          <select
            onChange={handleFormChange}
            required
            name="technician"
            value={formData.technician}
            className="form-control"
          >
            <option value={0}>Choose a technician</option>
            {technicians.map((technician) => (
              <option key={technician.id} value={technician.id}>
                {technician.first_name} {technician.last_name}
              </option>
            ))}
          </select>
        </div>
        <div className="form-group form-floating col-5 mx-auto mt-3" style={{ width: 300 }}>
          <input onChange={handleFormChange} value={formData.reason} required name="reason" type="text" className="form-control" />
          <label htmlFor="reason">Reason</label>
        </div>
        <div className="d-grid form group col-5 mx-auto" style={{ width: 300 }}>
          <button className="btn btn-primary btn-block mt-3">Create</button>
        </div>
      </form>

    </div>
  )
}
export default AppointmentForm
