import React, { useEffect, useState } from "react";
const ServiceHistory = () => {
  const [appointments, setAppointments] = useState([]);
  const [status, setStatus] = useState("");

  const vinSearch = async (e) => {
    const value = e.target.value;
    setStatus(value);
  };

  const handleVinSearch = async (e) => {
    e.preventDefault();
    const filtered = appointments.filter((appointment) =>
    appointment.vin.includes(status))
    setAppointments(filtered)
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("http://localhost:8080/api/appointments/");
        if (response.ok) {
          const data = await response.json();
          setAppointments(data.appointments);
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, []);
  return (
    <div>
      <h1>Service History</h1>
      <div className="search">
        <form className="input" onSubmit={handleVinSearch}>
          <input placeholder="Search by VIN..." 
          value={status}
          onChange={vinSearch} />
          <button type="suubmit" className="btn btn-primary">
            Search
          </button>
        </form>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <td>VIN</td>
            <td>Is VIP</td>
            <td>Customer</td>
            <td>Date</td>
            <td>Time</td>
            <td>Technician</td>
            <td>Reason</td>
            <td>Status</td>
          </tr>
        </thead>
        <tbody className="table-group-divider">
          {appointments.map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.vip ? "Yes" : "No"}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date}</td>
                <td>{appointment.time}</td>
                <td>
                  {" "}
                  {appointment.technician.first_name}{" "}
                  {appointment.technician.last_name}
                </td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
export default ServiceHistory;
